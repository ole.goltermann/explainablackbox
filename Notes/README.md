# What does it mean to explain a 'black box'

***
Period: February, 2022 - June, 2022 <br>

Project members: Ole Goltermann, Rena Bayramova |  ...
***

A collection of different perspectives on the topic of explainable AI and what it means to explain a so called black box.

## Meeting notes

### 14.02.2022 - Define project idea

To do:
1) Go through the research on the application of deep learning to diagnosis and prognosis in clinical cases and narrow down to 2 (Rena)
2) Eliminate the irrelevant papers from the existing list (Ole)
3) Check the current practice in regards to the topic in prominent labs. And try to find if there are any implementing our recommendations
4) Create a file where we collect strong opinions from different researchers - both those we agree and disagree with
5) Each of us adds one paper as an educational resource to be on the same page on how deep learning works

### 20.02.2022 - Define project idea II

**Agenda**:
- discuss the opinions from the file
- Select the topic we want to focus on: e.g. as outcome provide a guideline how to use xAI in a research project 
- talk about how we can include the others from the cohort 
- ask JD Haynes to join

**Notes**:

- When do we recommend to introduce a change in the way deep learning models are implemented? For instance, if we are dealing with brain data, we can either choose to focus on less complex datasets to start with, including preprocessed scans or specific brain features and try to answer the question we have at hand (e.g., classification) and check the performance. In case we are not satisfied with it, we can gradually expand on the number of features we include and check the performance again. An important point here will be to decide the acceptable threshold (defined, for example, by information criteria) depending on the domain. Like in Occam's razor, if a more complex model does not increase the threshold, you should go with the simpler one. Now, if you go with the more complex model, and the input of this model is not at the feature level ... (let's think about this more, for now, Ole defined it as using raw mri data vs cortical thickness mri data)
- Think more about the threshold and how its value can change depending on the domain. We discussed individual variability in clinical patients that could make it harder to find the best performing model (i.e., decide the number of features) vs other domains where variability and its relevance may be less significant.
- Choose the explanations method that can allow you to simplify your input to more general features
- Step 3 would be going back from the complex level by looking at the weight each feature holds for our prediction and possibly exclude the least informative features. 
- We focus on brain research and predictions made from human imaging data
- we should maybe try to come up with a working definiton of what is an explanation



**Todo**:

- Finalize the abstract by Wednesday and send it to JD Haynes



### XX.02.2022 - Kick Off Meeting

