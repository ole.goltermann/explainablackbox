# What does it mean to explain a 'black box'?

`[Last update: 2022-02-22]`

***
Period: February, 2022 - June, 2022 <br>

Project members: Ole Goltermann, Rena Bayramova | Lioba Enk, Max Andreas Bosse Hinrichs, Fabian Kamp, Bianca Serio, and Simon M. Hofmann
***

A collection of different perspectives on the topic of explainable AI and what it means to explain a so called black box. 

## Papers and links

All papers are stored [here](https://owncloud.gwdg.de/index.php/s/CLxCr8S3jBBhaDU) <br>
For password access text Ole (ole.goltermann@maxplanckschools.de) <br>

[Poster Abstract](https://docs.google.com/document/d/1ioIaQym8I6gONdNU7akWFF53yxs2lhJdU6hzulv4Mu8/edit#) <br>
For access to the file text Ole (ole.goltermann@maxplanckschools.de)

**Review Paper 1** (Ole) <br>
[Samek et al. 2021](https://ieeexplore.ieee.org/document/9369420) <br>
Explaining Deep Neural Networks and Beyond: A Review of Methods and Applications <br>

**Review Paper 2** (Rena) <br>
[Hoffmann et al. 2019](https://arxiv.org/abs/1812.04608) <br>
Metrics for Explainable AI: Challenges and Prospects <br>

[**Opinions file**](https://docs.google.com/spreadsheets/d/1RqcSkgGKyv2lsrWDb9iW7SMZfp3X1MXpZZZHL8qpTKM/edit?usp=sharing)

[**Short summary drafted by Ole and Rena, 01 July 2022**](https://docs.google.com/document/d/10jK_7Y_nzCd5XYYRe9Vr-zIuoyYHjuhBwTfbyJRzw5A/edit)

**Links:**

https://ec.europa.eu/research-and-innovation/en/horizon-magazine/opening-black-box-artificial-intelligence <br>
https://towardsdatascience.com/opening-the-black-box-an-explanation-of-explainable-ai-7024d8b1f6b3 <br>
https://www.scientificamerican.com/article/demystifying-the-black-box-that-is-ai/ <br>
https://towardsdatascience.com/black-box-and-white-box-models-towards-explainable-ai-172d45bfc512 <br>
https://bigcloud.global/the-difference-between-white-box-and-black-box-ai/ <br>
https://hdsr.mitpress.mit.edu/pub/f9kuryi8/release/6 <br>
https://www.analyticsinsight.net/top-20-artificial-intelligence-research-labs-in-the-world-in-2021/ <br>


## Meeting notes
All notes can be found [here](https://pad.gwdg.de/wmU2RNYvTnK7Yq3KIPTvvA?view) or [here](https://gitlab.gwdg.de/ole.goltermann/explainablackbox/-/tree/main/Notes) if you don't have access to pad

**Meeting link:**

https://ucl.zoom.us/j/96224586351 Meeting ID: 962 2458 6351 Passcode: 

[**Journal Club Presentations**](https://gitlab.gwdg.de/ole.goltermann/explainablackbox/-/tree/main/journalclub)

**Notes on discussed literature**

[Here](https://docs.google.com/spreadsheets/d/1dU4k5lixaUR9VwVPRUN3pVOsNXSR1Yvg/edit#gid=1656021112) you can find the link to the whiteboard with take-home messages and other notes on the discussed literature.


